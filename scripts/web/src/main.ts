import { Chart, registerables } from 'chart.js';
import { routing } from './tools/routing.ts';
import './style.css';
import { setActiveTab } from './tools/setActiveTab.ts';

Chart.register(...registerables);

window.addEventListener('popstate', routing);
setActiveTab();
setTimeout(routing, 0);
