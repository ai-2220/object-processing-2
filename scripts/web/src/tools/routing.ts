import { cvTransform } from './cvTransform';
import { loadFile } from './loadFile';
import Ananas from '../static/ananas.jpg';
import SaulGoodman from '../static/Saul_goodman.mp4';
import { RouterType } from './utils';
import { videoProcessing } from './videoProcessing';

const app = document.getElementById('app') as HTMLDivElement;

const routes: RouterType[] = [
  {
    url: '',
    callback: async () => {
      await fetch('src/templates/image-page.html')
        .then(async (response) => {
          app.innerHTML = await response.text();
        })
        .catch((err) => console.error(err));

      loadFile(document.querySelector<HTMLInputElement>('#input-file')!);
      const image = document.getElementById('cv-image') as HTMLImageElement;
      image.src = Ananas;

      cvTransform(image);
    },
  },
  {
    url: 'video',
    callback: async () => {
      await fetch('src/templates/video-page.html')
        .then(async (response) => {
          app.innerHTML = await response.text();
        })
        .catch((err) => console.error(err));

      const video = document.getElementById('cv-video') as HTMLVideoElement;
      video.src = SaulGoodman;

      videoProcessing(video);
    },
  },
  {
    url: 'camera',
    callback: async () => {
      await fetch('src/templates/camera-page.html')
        .then(async (response) => {
          app.innerHTML = await response.text();
        })
        .catch((err) => console.error(err));

      const video = document.getElementById('cv-video') as HTMLVideoElement;
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: false })
        .then((stream) => {
          video.srcObject = stream;
          video.play();
        })
        .catch((err) => console.error(err));

      videoProcessing(video);
    },
  },
];

export const routing = () => {
  let hash = window.location.hash.substring(1).replace(/\//gi, '/');

  let route = routes[0];

  for (let i = 0; i < routes.length; i++) {
    let taskRoute = routes[i];
    if (hash === taskRoute.url) {
      route = taskRoute;
    }
  }
  route.callback();
  history.replaceState({}, document.title, !!route.url.length ? route.url : '.');
};
