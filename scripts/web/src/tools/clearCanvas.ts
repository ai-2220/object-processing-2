import { CANVAS_NAMES } from "./utils";

export const clearCanvas = () => {
  CANVAS_NAMES.map((name) => {
    const canvas = document.getElementById(name) as HTMLCanvasElement;
    let context = canvas.getContext("2d");
    context?.clearRect(0, 0, canvas.width, canvas.height);
  });
};
