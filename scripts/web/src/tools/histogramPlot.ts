import cv from '@techstark/opencv-js';
import { Chart, ChartItem } from 'chart.js';

// @ts-ignore
window.cv = cv;

let chart: Chart<'line', Uint8Array, number> | null = null;

export const histogramPlot = (image: cv.Mat) => {
  if (chart) {
    chart.clear();
    chart.destroy();
  }

  let bgrPlanes = new cv.MatVector();
  cv.split(image, bgrPlanes);

  let bHist: any = new cv.Mat();
  let gHist: any = new cv.Mat();
  let rHist: any = new cv.Mat();
  cv.calcHist(bgrPlanes as unknown as cv.Mat, [0], new cv.Mat(), bHist, [256], [0, 255], false);
  cv.calcHist(bgrPlanes as unknown as cv.Mat, [1], new cv.Mat(), gHist, [256], [0, 255], false);
  cv.calcHist(bgrPlanes as unknown as cv.Mat, [2], new cv.Mat(), rHist, [256], [0, 255], false);

  //   cv.normalize(bHist, bHist, 0, image.rows, cv.NORM_MINMAX, -1, new cv.Mat());
  //   cv.normalize(bHist, gHist, 0, image.rows, cv.NORM_MINMAX, -1, new cv.Mat());
  //   cv.normalize(bHist, rHist, 0, image.rows, cv.NORM_MINMAX, -1, new cv.Mat());
  chart = new Chart(document.getElementById('histogram-chart') as ChartItem, {
    data: {
      datasets: [
        {
          type: 'line',
          label: 'blue',
          data: bHist.data,
          backgroundColor: 'blue',
          borderColor: 'blue',
        },
        {
          type: 'line',
          label: 'green',
          data: gHist.data,
          backgroundColor: 'green',
          borderColor: 'green',
        },
        {
          type: 'line',
          label: 'red',
          data: rHist.data,
          backgroundColor: 'red',
          borderColor: 'red',
        },
      ],
      labels: Array(rHist.data.length)
        .fill(0)
        .map((_, index) => index + 1),
    },
    options: {
      normalized: false,
      responsive: true,
    },
  });
  bHist.delete();
  gHist.delete();
  rHist.delete();
};
