export interface RouterType {
  url: string;
  callback: () => void;
}

export const FPS = 30;

export const CANVAS_NAMES = [
  "canvas-hsv",
  "canvas-canny",
  "canvas-gaus",
  "canvas-substraction",
];
