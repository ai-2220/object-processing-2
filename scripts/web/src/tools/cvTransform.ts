import cv from '@techstark/opencv-js';
import { histogramPlot } from './histogramPlot';
import { clearCanvas } from './clearCanvas';

// @ts-ignore
window.cv = cv;

export const cvTransform = (imgRef: HTMLElement | cv.Mat) => {
  clearCanvas();
  let image = null;

  if ('col' in imgRef) {
    image = imgRef.clone();
  } else {
    image = cv.imread(imgRef);
  }
  let hsv: any = new cv.Mat();
  let bgr: any = new cv.Mat();
  let gray: any = new cv.Mat();
  let canny: any = new cv.Mat();
  let gaus: any = new cv.Mat();
  let substraction: any = new cv.Mat();

  cv.cvtColor(image, bgr, cv.COLOR_RGBA2BGR);
  cv.cvtColor(bgr, hsv, cv.COLOR_BGR2HSV);
  cv.cvtColor(bgr, gray, cv.COLOR_BGR2GRAY);
  cv.Canny(gray, canny, 165, 255);

  cv.GaussianBlur(gray, gaus, new cv.Size(5, 5), 0);
  cv.Canny(gaus, gaus, 165, 255);

  cv.GaussianBlur(gray, substraction, new cv.Size(49, 49), 0);
  cv.subtract(gray, substraction, substraction);

  if ('scroll' in imgRef) {
    histogramPlot(bgr);
  }
  cv.imshow('canvas-hsv', bgr);
  cv.imshow('canvas-canny', canny);
  cv.imshow('canvas-gaus', gaus);
  cv.imshow('canvas-substraction', substraction);
  hsv.delete();
  bgr.delete();
  gray.delete();
  canny.delete();
  gaus.delete();
  substraction.delete();
};
