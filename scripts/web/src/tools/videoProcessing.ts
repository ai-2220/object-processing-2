import cv from '@techstark/opencv-js';
import { cvTransform } from './cvTransform';
import { FPS } from './utils';

// @ts-ignore
window.cv = cv;

export const videoProcessing = (videoSource: HTMLVideoElement) => {
  let cap = new cv.VideoCapture(videoSource);
  let src: any = new cv.Mat(videoSource.height, videoSource.width, cv.CV_8UC4);
  let timer: any = null;
  let location = window.location.href.split(window.location.host)[1];

  const readVideo = () => {
    try {
      if (timer && window.location.href.split(window.location.host)[1] !== location) {
        clearTimeout(timer);
        src.delete();
        return;
      }
      let begin = Date.now();
      cap.read(src);
      cvTransform(src);
      setTimeout(readVideo, 1000 / FPS - (Date.now() - begin));
    } catch (error) {
      console.error(error);
    }
  };
  timer = setTimeout(readVideo, 0);
};
