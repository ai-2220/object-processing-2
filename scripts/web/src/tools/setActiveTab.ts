export const setActiveTab = () => {
  const lis = document.querySelectorAll("li");
  lis.forEach((item) => {
    item.addEventListener("click", () => {
      let current = document.getElementsByClassName("active");
      if (current) {
        if (item.classList.contains("logo")) {
          return;
        }
        current[0].classList.remove("active");
        item.classList.add("active");
      }
    });
  });
};
