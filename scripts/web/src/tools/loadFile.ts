import { cvTransform } from './cvTransform';

export const loadFile = (element: HTMLInputElement) => {
  const image = document.getElementById('cv-image') as HTMLImageElement;
  const textElement = document.getElementsByClassName('input-file-text')[0] as HTMLSpanElement;
  element.addEventListener('change', (e) => {
    const files = (<HTMLInputElement>e.target).files;
    if (files?.length) {
      image.src = URL.createObjectURL(files[0]);
      textElement.innerHTML = files[0].name;
    }
  });
  image.onload = () => {
    cvTransform(image);
  };
};
