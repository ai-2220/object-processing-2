﻿open Emgu.CV
open Emgu.CV.Structure
open System.Drawing
open System

//Получаем исходное изображение
let imagePath = $"""{Environment.CurrentDirectory.Split("\\bin")[0]}\\Pictures\\cats.jpg"""
let image = new Image<Bgr, byte>(imagePath)

//Инициализируем ядро размером 5 на 5
let customFilter = new Matrix<float>(5, 5)

//Заполняем матрицу значениями
customFilter.[0, 0] <- -1
customFilter.[0, 1] <- -1
customFilter.[0, 2] <- -1
customFilter.[0, 3] <- -1
customFilter.[0, 4] <- -1
 
customFilter.[1, 0] <- -1
customFilter.[1, 1] <- -0.5
customFilter.[1, 2] <- -0.5
customFilter.[1, 3] <- -0.5
customFilter.[1, 4] <- -1

customFilter.[2, 0] <- -1
customFilter.[2, 1] <- -0.5
customFilter.[2, 2] <- 20
customFilter.[2, 3] <- -0.5
customFilter.[2, 4] <- -1

customFilter.[3, 0] <- -1
customFilter.[3, 1] <- -0.5
customFilter.[3, 2] <- -0.5
customFilter.[3, 3] <- -0.5
customFilter.[3, 4] <- -1

customFilter.[4, 0] <- -1
customFilter.[4, 1] <- -1
customFilter.[4, 2] <- -1
customFilter.[4, 3] <- -1
customFilter.[4, 4] <- -1

//Инициализируем результативное изображение
let filteredImage = new Image<Bgr, byte>(image.Size)

//Применяем метод Filter2D
CvInvoke.Filter2D(image, filteredImage, customFilter, new Point(-1, -1))

CvInvoke.Imshow("Original", image)
CvInvoke.Imshow("Filtred", filteredImage)

CvInvoke.WaitKey(0)

