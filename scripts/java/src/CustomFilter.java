import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CustomFilter {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {

        Mat source = Imgcodecs.imread("object-processing-2/scripts/java/pic/quznSQqW-dU.jpg");

        if (source.empty()) {
            System.out.println("Не удалось загрузить изображение.");
            return;
        }

        // Создание своей собственной матрицы фильтра
        Mat kernel = new Mat(3, 3, CvType.CV_32F)
        {
            {
                put(0, 0, 1, -1.45, 1);
                put(1, 0, -2, 5, -2);
                put(2, 0, 1, -1.45, -1);

            }
        };

        // Применение фильтра к изображению
        Mat dest = new Mat();
        Mat canny = new Mat();
        Imgproc.filter2D(source, dest, -1, kernel);
        Imgproc.Canny(source, canny,30,100);

        // Отображение исходного и обработанных изображений
        displayImage(source, "Исходное изображение");
        displayImage(dest, "Изображение после применения фильтра");
        displayImage(canny, "Изображение после применения Canny");
    }

    // Функция отображения изображения в окне
    private static void displayImage(Mat image, String windowTitle)
    {
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".jpg", image, matOfByte);
        byte[] byteArray = matOfByte.toArray();

        ImageIcon imageIcon = new ImageIcon(byteArray);
        JFrame frame = new JFrame(windowTitle);
        JLabel label = new JLabel(imageIcon);
        frame.add(label);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
