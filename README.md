# ПЗ2 по обработке изображений

## Состав репозитория

В [static](static/) хранятся изображения, видосы и гифки.
В [scripts](scripts/) хранятся код и блокноты

## :star2: Плановое задание

Задание выполнено на функциональном C#, а именно на F#.

### Алгоритм работы скрипта

Запуск программы
```
git clone https://gitlab.com/ai-2220/object-processing-2.git

cd ./scripts/matrixF

dotnet run
```
### Ядро имеет такие значения
Скрипт для получения графика находится в блокноте [3d_matrix.ipynb](scripts/3d-matrix/3d_matrix_filter3d.ipynb)
<div style="width: 1000px; display: flex; flex-direction: column;">
    <img src="static/matrix.jpg" width="200px" />
    <img src="static/3d-matrix.png" width="300px" />
</div>

### Основной код

```
//Получаем исходное изображение
let imagePath = $"""{Environment.CurrentDirectory.Split("\\bin")[0]}\\Pictures\\cats.jpg"""
let image = new Image<Bgr, byte>(imagePath)

//Инициализируем ядро размером 5 на 5
let customFilter = new Matrix<float>(5, 5)

//Заполняем матрицу значениями
customFilter.[0, 0] <- -1
customFilter.[0, 1] <- -1
customFilter.[0, 2] <- -1
customFilter.[0, 3] <- -1
customFilter.[0, 4] <- -1
 
customFilter.[1, 0] <- -1
customFilter.[1, 1] <- -0.5
customFilter.[1, 2] <- -0.5
customFilter.[1, 3] <- -0.5
customFilter.[1, 4] <- -1

customFilter.[2, 0] <- -1
customFilter.[2, 1] <- -0.5
customFilter.[2, 2] <- 20
customFilter.[2, 3] <- -0.5
customFilter.[2, 4] <- -1

customFilter.[3, 0] <- -1
customFilter.[3, 1] <- -0.5
customFilter.[3, 2] <- -0.5
customFilter.[3, 3] <- -0.5
customFilter.[3, 4] <- -1

customFilter.[4, 0] <- -1
customFilter.[4, 1] <- -1
customFilter.[4, 2] <- -1
customFilter.[4, 3] <- -1
customFilter.[4, 4] <- -1

//Инициализируем результативное изображение
let filteredImage = new Image<Bgr, byte>(image.Size)

//Применяем метод Filter2D
CvInvoke.Filter2D(image, filteredImage, customFilter, new Point(-1, -1))
```

### Результаты
<div style="display: flex; flex-direction: row; column-gap: 20px">
    <img src="static/cats.png" width="300px" />
    <img src="static/drain-cats.png" width="300px" />
</div>

## Аналогичное задание, выполненное на :nail_care: Java :nail_care:
Загрузка исходного изображения <br/>
``Mat source = Imgcodecs.imread("object-processing-2/scripts/java/pic/quznSQqW-dU.jpg");``

Инициализация и заполнение матрицы
```
Mat kernel = new Mat(3, 3, CvType.CV_32F)
        {
            {
                put(0, 0, 1, -1.45, 1);
                put(1, 0, -2, 5, -2);
                put(2, 0, 1, -1.45, -1);

            }
        };
```

Применение фильтров к изображению
```
Mat dest = new Mat();
Mat canny = new Mat();
Imgproc.filter2D(source, dest, -1, kernel);
Imgproc.Canny(source, canny,30,100);
```

Отображение исходного и обработанных изображений
```
displayImage(source, "Исходное изображение");
displayImage(dest, "Изображение после применения фильтра");
displayImage(canny, "Изображение после применения Canny");
```
### Результаты
<div style="display: flex; flex-direction: row; column-gap: 20px">
    <img src="scripts/java/pic/quznSQqW-dU.jpg" width="300px" />
    <img src="static/unbelievable-mad-cat.jpg" width="300" />
</div>
<br/>

### Результат применения собственного фильтра в сравнении с реализованным в open cv (Canny)
<div style="display: flex; flex-direction: row; column-gap: 20px">
    <img src="static/unbelievable-mad-cat.jpg" width="300" />
    <img src="static/unbelievable-canny-cat.jpg" width="300px" />
</div>


## :dizzy: Задание на плюсик

Сделано с любовью на **javascript**, но на немомного крутом **javascript** - **typescript**! Оно умеет в типизацию и даже подсказавает разработчикам и направляет их в решение их же проблем и проблем с **javascript**

Ну и собственно в лучших традициях - никаких фремворках, только голый vanilla js, только боль!

### Алгоритм работы скрипта

Запуск сайта

```
git clone https://gitlab.com/ai-2220/object-processing-2.git

cd ./scripts/web

docker-compose up
```

#### Обработка изображения

Обработка изображений расположена в файле [cvTransform.ts](scripts/web/src/tools/cvTransform.ts).

```
// Очищаем canvas на странице
clearCanvas();
let image = null;

//Проверка на тип аргумента функции, если аргумент - экземплер класса cv.Mat, то у него есть метод col и мы копируем
//его в объект image, иначе считываем ссылку через cv.imread
if ("col" in imgRef) {
    image = imgRef.clone();
} else {
    image = cv.imread(imgRef);
}

//Инициализируем классы cv.Mat, крч всё как в плюсах тут
let hsv = new cv.Mat();
let bgr = new cv.Mat();
let gray = new cv.Mat();
let canny = new cv.Mat();
let gaus = new cv.Mat();
let substraction = new cv.Mat();

cv.cvtColor(image, bgr, cv.COLOR_RGBA2BGR);
cv.cvtColor(bgr, hsv, cv.COLOR_BGR2HSV);
cv.cvtColor(bgr, gray, cv.COLOR_BGR2GRAY);
cv.Canny(gray, canny, 165, 255);

cv.GaussianBlur(gray, gaus, new cv.Size(5, 5), 0);
cv.Canny(gaus, gaus, 165, 255);

cv.GaussianBlur(gray, substraction, new cv.Size(49, 49), 0);
cv.subtract(gray, substraction, substraction);

//Аргумент функции может быть html элементом типа image, для него выводим гистограмму
if ("scroll" in imgRef) {
    histogramPlot(bgr);
}

//В метод cv.imshow передаём id canvas элемента и cv.Mat объект, который мы заполнили ранее
cv.imshow("canvas-hsv", bgr);
cv.imshow("canvas-canny", canny);
cv.imshow("canvas-gaus", gaus);
cv.imshow("canvas-substraction", substraction);
```

#### Построение гистограммы

В файле [histogramPlot](scripts/web/src/tools/histogramPlot.ts) находится алгоритм построения гистрограммы.

Копирую изображение в cv.MatVector, где оно разбивается на 3 массива ['b', 'g', 'r'], и затем для каждого элемента высчитываю гистограмму с помощью функции cv.calcHist().

Для построения гистограммы использую библиотеку Chart.js

#### Обработка видео/потока камеры

Алгоритм обработки видео и потока с камеры находится в файле [videoProcessing](scripts/web/src/tools/videoProcessing.ts).

```
//Считываю поток фреймов через ссылку на html элемент
let cap = new cv.VideoCapture(videoSource);
let src = new cv.Mat(videoSource.height, videoSource.width, cv.CV_8UC4);
//Нужно для очистки функции из микрозадач, если этого не делать, то очередь забивается и функция будет работать всегда
let timer: any = null;
//URL страницы, да, да, я сделал SPA для этой задачки...
let location = window.location.href.split(window.location.host)[1];

const readVideo = () => {
    try {
        if (timer && window.location.href.split(window.location.host)[1] !== location) {
            clearTimeout(timer);
            return;
        }
        let begin = Date.now();
        cap.read(src);
        //Уже знакомая функция
        cvTransform(src);
        //Второй аргумент - время через которое появится следующий фреймя на видео
        setTimeout(readVideo, 1000 / FPS - (Date.now() - begin));
    } catch (error) {
        console.error(error);
    }
};

timer = setTimeout(readVideo, 0);
```

Остальные функции чисто js-овские приколы

### Результаты

#### Обработка изображений

<img src="static/web-first-page.jpg" width="500px" />

#### Построение гистограммы

<img src="static/web-histogram.jpg" width="500px" />

#### Обработка видео

<img src="static/web-second-page.jpg" width="500px" />

#### Обработка потока камеры

<img src="static/web-third-page.jpg" width="500px" />
